export const keyMirror = (prefix, obj) => {
    const ret = {};
    if (!obj instanceof Object) {
        throw Error('keyMirror(...): Second argument must be an object');
    }
    for (let key in obj) {
        if (!obj.hasOwnProperty(key)) {
            continue;
        }
        ret[key] = `${prefix}_${key}`;
    }
    return ret;
}

export const toISOStringWithTz = (dateObj) => {
    const tzo = -dateObj.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = (num) => {
            const norm = Math.floor(Math.abs(num))
            return (norm < 10 ? '0' : '') + norm
        }
    return dateObj.getFullYear() +
        '-' + pad(dateObj.getMonth() + 1) +
        '-' + pad(dateObj.getDate()) +
        'T' + pad(dateObj.getHours()) +
        ':' + pad(dateObj.getMinutes()) +
        ':' + pad(dateObj.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60)
}
import React from 'react'
import styles from './index.module.scss'

const NavButton = ({onClick, name}) => (
    <button className={styles.navBtn} onClick={onClick} >{name}</button>
)
export default NavButton
import React from 'react'
import styles from './index.module.scss'

const Base = (type, {onClick, onSubmit, value}) => (
    <input
        className={styles.btn}
        type={type}
        onClick={onClick}
        onSubmit={onSubmit}
        value={value}
    />
)

export const Button = Base.bind(null, 'button')
export const SubmitButton = Base.bind(null, 'submit')

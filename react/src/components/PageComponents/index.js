import React from 'react'
import { NavLink } from 'react-router-dom'

import map from 'lodash/map'

import styles from './index.module.scss'

export const NavBar = ({links}) => (
    <nav>
            {map(links, (link) => (
                    <NavLink key={link.to} className={styles.link} activeClassName={styles.active} to={link.to} >{link.name}<span className={styles._} ></span></NavLink>
            ))}
    </nav>
)

export const Footer = () => (
    <footer>
    </footer>
)

export const Header = ({children}) => (
    <header>
        {children}
    </header>
)
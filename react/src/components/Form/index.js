import React, { useState } from 'react'

import assign from 'lodash/assign'
import each from 'lodash/each'
import map from 'lodash/map'
import range from 'lodash/range'

import InputField from '../InputField'
import { SubmitButton } from '../Buttons'
import DropDown from '../DropDown'

import styles from './index.module.scss'

const Form = ({schema, onSubmit, children, name, submitName}) => {
    const [items, setItems] = useState([...schema])


    const handleInputChange = (e) => {
        const idx = e.target.dataset.index
        if (!idx.includes('.')) {
            setItems(Object.assign([], items, {
                [idx]: Object.assign({}, items[idx], {
                    value: e.target.value
                })
            }))
        } else {
            const [mainIdx, subIdx] = idx.split('.')
            const newItems = items.slice()
            newItems[mainIdx].value = Object.assign([], newItems[mainIdx].value, {
                [subIdx]: e.target.value
            })
            setItems(newItems)
        }
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        const newItems = items.slice()
        let judge = true
        each(newItems, (item, idx) => {
            if (item.type === 'dropDown') return true

            if (!item.name.endsWith('[]')) {
                if (item.value === undefined || item.value === '') {
                    judge = false
                    item.isValid = null
                } else if (item.regex !== undefined && !item.regex.test(item.value)) {
                    judge = false
                    item.isValid = false
                } else {
                    item.isValid = true
                }
            } else {
                item.value = assign(Array(item.iteration).fill(), item.value)
                item.isValid = Array(item.iteration).fill()

                each(item.value, (el, jdx) => {
                    if (el === undefined) {
                        judge = false
                        item.isValid[jdx] = null
                    } else if (item.regex !== undefined && !item.regex.test(el)) {
                        judge = false
                        item.isValid[jdx] = false
                    } else {
                        item.isValid[jdx] = true
                    }
                })
            }
        })
        setItems(newItems)
        if (!judge) return
        onSubmit(e)
    }

    return (
        <form className={styles.form} onSubmit={handleSubmit}>
            <h2>{name}</h2>
            {map(items, (field, idx) => {
                return (
                    <div key={field.name} className={styles.fd}>
                        <div className={styles.fdName}>
                            {field.prettyName} 
                        </div>
                        {   
                            !field.name.endsWith('[]') ?
                            <div className={styles.fdInput}>
                                {
                                    field.type === 'dropDown' ?
                                    <DropDown
                                        name={field.name}
                                        items={field.items}
                                        onChange={field.onChange}
                                    /> :
                                    <>
                                    <InputField
                                        index={`${idx}`}
                                        name={field.name}
                                        type={field.type}
                                        isValid={field.isValid === undefined ? true : field.isValid}
                                        onChange={handleInputChange}
                                    />
                                    <span className={styles.err}>{
                                        field.isValid === null ?
                                        '입력이 필요합니다.' :
                                        field.isValid === false ?
                                        '입력형식이 유효하지 않습니다.' : null
                                    }</span>
                                    </>
                                } 

                            </div> : map(range(field.iteration), (v, jdx) => (
                                <div className={styles.fdInput}>
                                    <InputField
                                        key={v}
                                        index={`${idx}.${jdx}`}
                                        name={field.name}
                                        type={field.type}
                                        isValid={field.isValid === undefined ? true : field.isValid[jdx]}
                                        onChange={handleInputChange}
                                    />
                                    <span className={styles.err}>{
                                        field.isValid === undefined ?
                                        null :
                                        field.isValid[jdx] === null ?
                                        '입력이 필요합니다.' :
                                        field.isValid[jdx] === false ?
                                        '입력형식이 유요하지 않습니다.' : null
                                    }</span>
                                </div>
                            ))
                        }
                    </div>
                )
            })}
            {children ? children : null}
            <SubmitButton value={submitName} />
        </form>
    )
}

export default Form

import React from 'react'

import styles from './index.module.scss'
import classNames from 'classnames/bind'

const cx = classNames.bind(styles)

const InputField = ({index, name, type, isValid, onChange}) => {
    return (
        <input
            className={cx('field', {'invalid':!isValid})}
            data-index={index}
            name={name}
            type={type}
            onChange={onChange}
            autoComplete={'off'}
        />
    )
}

export default InputField
import React from 'react'
import styles from './index.module.scss'


const DMCLabelList = ({ dmcLabelData }) => {
    return (
        <div className={styles.outer} >
            {dmcLabelData.map((val, idx) => {
                return (
                    <React.Fragment key={val.id}>
                        <DMCLabel
                            dmcImg={val.dmcSrc}
                            dmcData={val.dmcData}
                        />
                        { (idx + 1) % 12 === 0 ? <div className="pb"></div> : null }
                    </React.Fragment>
                )
            })}
        </div>
    )
}

const DMCLabel = ({ dmcImg, dmcData }) => {
    const renderInfo = (obj) => {
        return (
            <div className={styles.table}>
                {obj.map((el, idx) => {
                    const key = el[0]
                    const val = el[1] ? el[1].replace('-', '‑') : el[1]
                    return (
                        <div key={key} className={styles.row}>
                            <div className={styles.cell}>{`${key}: `}</div>
                            <div className={styles.cell}>{val}</div>
                        </div>
                    )
                })}
            </div>
        )
    }

    const data1 = Object.entries(dmcData)
    const data2 = data1.splice(7)
    const data3 = data2.splice(6)
    const data4 = data3.splice(3).slice(0, 3)

    return (
        <div className={styles.wrap}>
            <div className={styles.dmc}>
                <img src={dmcImg} alt=""/>
            </div>
            <div className={styles.info}>
                {renderInfo(data1)}
                {renderInfo(data2)}
            </div>
            <div className={styles.infoBottom}>
                {renderInfo(data3)}
                {renderInfo(data4)}
            </div>
            <div className={styles.rohs}>
                RoHS
            </div>
        </div>
    )
}

export default DMCLabelList
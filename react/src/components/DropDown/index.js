import React from 'react'
import map from 'lodash/map'
import styles from './index.module.scss'

const DropDown = ({items, name, onChange}) => (
    <select name={name} className={styles.dropDown} onChange={onChange}>
        {map(items, (item) => (
            <option key={item.id} value={item.value}>{item.name}</option>
        ))}
    </select>
)

export default DropDown
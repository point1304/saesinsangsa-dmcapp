import React from 'react'
import ReactDOM from 'react-dom'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { createStore, applyMiddleware } from 'redux'
import { Provider as StoreProvider } from 'react-redux'

import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas'
import rootReducer from './reducers'

import { Footer } from './components/PageComponents'
import HeaderContainer from './containers/HeaderContainer'
import LoginRequired from './containers/LoginRequired'

import DMCsPage from './pages/DMCsPage'
import CreateOrderPage from './pages/CreateOrderPage'
import AuthPage from './pages/AuthPage'
import NotFoundPage from './pages/NotFoundPage'

import 'normalize.css'
import './index.scss'

class App extends React.Component {
    render() {
        return (
            <Router>
                <HeaderContainer />
                <main>
                    <div className={'contents'}>
                        <Switch>
                            <Route exact path="/" component={AuthPage} />
                            <Route path="/createOrder" component={LoginRequired(CreateOrderPage)} />
                            <Route path="/dmcs" component={LoginRequired(DMCsPage)} />
                            <Route component={NotFoundPage} />
                        </Switch>
                    </div>
                </main>
                <Footer />
            </Router>
        )
    }
}


const sagaMiddleware = createSagaMiddleware()
const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
)
sagaMiddleware.run(rootSaga)

ReactDOM.render(
    <StoreProvider store={store}>
        <App />
    </StoreProvider>,
    document.getElementById('root')
)

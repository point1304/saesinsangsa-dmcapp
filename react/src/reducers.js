import { combineReducers } from 'redux'
import { actionTypes } from './actions'

/*
const defaultState = {
    selectedOrder: null,
    page: 1,
    perPage: 10,
    dmcsByOrderId: {
        1: {
            didFetch: false,
            isFetching: false,
            lastFetched: null,
            dmcs: []
        }
    },
    isLoggedIn : false,
    isCheckingLoginStatus : false,
    createOrderDidSucceed: null,
}
**/


function isLoggedIn(state = false, action) {
    switch (action.type) {
        case actionTypes.LOGIN.SUCCESS:
        case actionTypes.LOGIN_PING.SUCCESS:
            return true

        case actionTypes.LOGIN.FAIL:
        case actionTypes.LOGIN_PING.FAIL:
        case actionTypes.LOGOUT.SUCCESS:
            return false

        default:
            return state
    }
}

function isCheckingLogin(state = false, action) {
    switch (action.type) {
        case actionTypes.LOGIN_PING.REQUEST:
            return true

        case actionTypes.LOGIN_PING.SUCCESS:
        case actionTypes.LOGIN_PING.FAIL:
            return false
        
        default:
            return state
    }
}

function didCheckLogin(state = false, action) {
    switch (action.type) {
        case actionTypes.LOGIN_PING.REQUEST:
            return false
            
        case actionTypes.LOGIN_PING.SUCCESS:
        case actionTypes.LOGIN_PING.FAIL:
            return true
        
        default:
            return state
    }
}

function selectedOrder(state = null, action) {
    switch (action.type) {
        case actionTypes.FETCH_DMC_BY_ORDER_ID.REQUEST:
            return action.payload.orderId
            
        default:
            return state
    }
}

function createOrderDidSucceed(state = null, action) {
    switch (action.type) {
        case actionTypes.CREATE_DMC_ORDER.SUCCESS:
            return true

        case actionTypes.CREATE_DMC_ORDER.FAIL:
            return false

        default:
            return state
    }
}

function packages(state = {
    didFetch: false,
    lastFetched: null,
    isInvalid: false,
    dmcLabels: []
}, action) {
    switch (action.type) {
        case actionTypes.FETCH_DMC_BY_ORDER_ID.REQUEST:
            return {
                ...state,
            }

        case actionTypes.FETCH_DMC_BY_ORDER_ID.SUCCESS:
            return {
                ...state,
                didFetch: true,
                lastUpdated: new Date().getTime(),
                dmcLabels: action.payload.data,
            }

        case actionTypes.FETCH_DMC_BY_ORDER_ID.FAIL:
            return {
                ...state,
                isInvalid: true,
            }

        default:
            return state
    }
}

function dmcsByOrderId(state = {}, action) {
    switch (action.type) {
        case actionTypes.FETCH_DMC_BY_ORDER_ID.REQUEST:
        case actionTypes.FETCH_DMC_BY_ORDER_ID.SUCCESS:
        case actionTypes.FETCH_DMC_BY_ORDER_ID.FAIL:
            return {
                ...state,
                [action.payload.orderId]: packages(state[action.payload.orderId], action)
            }

        default:
            return state
    }
}

const rootReducer = combineReducers({
    selectedOrder,
    dmcsByOrderId,
    createOrderDidSucceed,
    isLoggedIn,
    didCheckLogin,
    isCheckingLogin
});

export default rootReducer

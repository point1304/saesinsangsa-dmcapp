import { call, put, select, takeEvery, all } from 'redux-saga/effects'
import {
    actionTypes,
    fetchDMCByOrderId,
    createDMCOrder,
    fetchDMCByClientCodeAndOrderNo,
    login,
    logout,
    loginPing } from './actions'

function* fetchDMCByOrderIdSaga(action) {
    const orderId = action.payload.orderId
    const state = yield select()
    //: caching
    if (state.dmcsByOrderId[orderId].didFetch) return

    try {
        const url = `/api/dmcs?orderId=${orderId}`
        const res = yield call(fetch, url, {credentials: 'include'})
        const data = yield call([res, res.json])

        yield put(fetchDMCByOrderId.success({ data, orderId }))

    } catch (error) {
        yield put(fetchDMCByOrderId.fail({ error, orderId }))
    }
}

function* createDMCOrderSaga(action) {
    try {
        const url = '/api/order'
        const headers = {
            'Content-Type': 'application/json',
        }
        const { body, history } = action.payload
        const res = yield call(
            fetch,
            url,
            {method: 'POST', headers, body, credentials: 'include'}
        )
        const orderId = yield call([res, res.json])

        yield put(createDMCOrder.success())
        history.push(`/dmcs/${orderId}`)

    } catch (error) {
        yield put(createDMCOrder.fail({ error }))
    }
}

function* fetchDMCByClientCodeAndOrderNoSaga(action) {
    const { clientOrderNo, clientCode, shippingNote, history } = action.payload

    try {
        const url = '/api/orderId' +
                    `?clientCode=${clientCode}` +
                    `&orderNo=${clientOrderNo}` +
                    `&shippingNote=${shippingNote}`

        const res = yield call(fetch, url, {credencials: 'include'})
        const orderId = yield call([res, res.json])

        yield put(fetchDMCByClientCodeAndOrderNo.success())

        history.push(`/dmcs/${orderId}`)

    } catch (error) {
        yield put(fetchDMCByClientCodeAndOrderNo.fail({ error }))
    }
}

function* loginSaga(action) {
    const { body } = action.payload

    try {
        const url = '/api/auth/login'
        const headers = {
            'Content-Type': 'application/json',
        }
        const res = yield call(fetch, url, {method: 'POST', headers, body})

        if (res.status === 200) {
            yield put(login.success())

        } else yield put(login.fail())

    } catch (error) {
        yield put(login.fail({ error }))
    }
}

function* loginPingSaga(action) {
    try {
        const url = '/api/auth/ping'
        const res = yield call(fetch, url, {credentials: 'include'})

        if (res.status === 200) {
            yield put(loginPing.success())

        } else {
            yield put(loginPing.fail())

        }
    
    } catch (error) {
        yield put(loginPing.fail({ error }))
        
    }
}

function* logoutSaga(action) {
    try {
        const url = '/api/auth/logout'
        const res = yield call(fetch, url, {credentials: 'include'})

        if (res.status === 200) {
            yield put(logout.success())
            yield put(loginPing.request({ history: action.payload.history }))

        } else {
            yield put(logout.fail({ status: res.status }))

        }
    } catch (error) {
        yield put(logout.fail({ error }))
    }
}

function* watchDMCSaga() {
    yield takeEvery(actionTypes.FETCH_DMC_BY_ORDER_ID.REQUEST, fetchDMCByOrderIdSaga)
    yield takeEvery(actionTypes.CREATE_DMC_ORDER.REQUEST, createDMCOrderSaga)
    yield takeEvery(actionTypes.FETCH_DMC_BY_CLIENT_CODE_AND_ORDER_NO.REQUEST, fetchDMCByClientCodeAndOrderNoSaga)
    yield takeEvery(actionTypes.LOGIN.REQUEST, loginSaga)
    yield takeEvery(actionTypes.LOGIN_PING.REQUEST, loginPingSaga)
    yield takeEvery(actionTypes.LOGOUT.REQUEST, logoutSaga)
}

export default function* rootSaga() {
    yield all([watchDMCSaga()])
}

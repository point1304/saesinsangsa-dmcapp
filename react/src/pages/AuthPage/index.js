import React from 'react'

import AuthForm from './AuthForm'

const AuthPage = ({history}) => (
    <AuthForm history={history} />
)

export default AuthPage
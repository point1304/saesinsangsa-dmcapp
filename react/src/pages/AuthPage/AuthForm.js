import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import Form from '../../components/Form'

import { login } from '../../actions'

//: caching
let schema = [
    {
        name: 'username',
        prettyName: '사용자 ID',
        type: 'text',
        regex: /^[a-z0-9]*$/
    },
    {
        name: 'password',
        prettyName: '비밀번호',
        type: 'password',
        regex: /^[a-zA-Z0-9 !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]*$/,
        size: 10,
    },
]

class AuthForm extends React.PureComponent {

    render() {
        const { isLoggedIn, onSubmit } = this.props
        return (
            isLoggedIn ?
            <Redirect to='createOrder' /> :
            <Form schema={schema} onSubmit={onSubmit} name="DMC관리 시스템" submitName="Sign in"/>
        )
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: state.isLoggedIn,
})
const mapDispatchToProps = (dispatch, ownProps) => ({
    onSubmit: (e) => {
        const formData = new FormData(e.target)
        const dataObj = Object.fromEntries(formData)
        const jsonData = JSON.stringify(dataObj)

        dispatch(login.request({ history: ownProps.history, match: ownProps.match, body: jsonData }))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthForm)
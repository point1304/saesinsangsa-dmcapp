import React from 'react'
import styles from './index.module.scss'

const NotFoundPage = () => (
    <div className={styles.wrap}>
        <h1>{'404 NotFound'}</h1>
        <h3>{'페이지를 찾을 수 없습니다.'}</h3>
        <p>
            {'아직 구현되지 않았거나'}
        <br/>
            {'유효하지 않은 주소입니다.'}
        </p>
    </div>
)
export default NotFoundPage
import React from 'react'

import each from 'lodash/each'
import transform from 'lodash/transform'

import Form from '../../components/Form'

class CreateOrderForm extends React.PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            schema: [{
                name: 'clientCode',
                prettyName: '고객사',
                type: 'dropDown',
                onChange: this.handleClientCodeDropDownChange.bind(this),
                items: []
            }, {
                name: 'clientOrderNo',
                prettyName: '고객사 주문번호 (Order No.)',
                type: 'dropDown',
                items: []
            }, {
                name: 'shipNote',
                prettyName: 'ASN번호 (Shipping Note)',
                type: 'text',
                regex: /^[a-zA-Z0-9]*$/
            }, {
                name: 'quantity',
                prettyName: '주문수량',
                type: 'number',
                regex: /^[0-9]*$/
            }, {
                name: 'manDate',
                prettyName: '납품제품 생산일',
                type: 'date',
            }],

            clientOrderNoDropDownItemsIndex: {},
            selectedClient: null,
        }
    }

    componentDidMount() {
        if ( localStorage.getItem('clientCodeDropDownItems') ) {
            const cachedClientCodeDropDownItems = 
                JSON.parse(localStorage.getItem('clientCodeDropDownItems'))
            const cachedClientOrderNoDropDownItemsIndex =
                JSON.parse(localStorage.getItem('clientOrderNoDropDownItemsIndex'))
            const cachedSelectedClient =
                JSON.parse(localStorage.getItem('selectedClient'))

            const newSchema = this.state.schema

            newSchema[0].items = cachedClientCodeDropDownItems || []
            newSchema[1].items = cachedClientOrderNoDropDownItemsIndex &&
                                    cachedSelectedClient ?
                                    cachedClientOrderNoDropDownItemsIndex[cachedSelectedClient] :
                                    []
            
            this.setState({
                schema                         : newSchema,
                selectedClient                 : cachedSelectedClient,
                clientOrderNoDropDownItemsIndex: cachedClientOrderNoDropDownItemsIndex || {}
            })
            
            return
        }

        const fetchClientDropDownItems = async() => {
            const res = await fetch('/api/client')
            const clientDropDownItems = await res.json()

            each(clientDropDownItems, (item) => {
                item.name = `${item.name} (${item.code})`
                item.value = item.code
                delete item.code
            })

            const initialClientDropDownValue = clientDropDownItems[0].value
            const clientOrderNoDropDownItems = 
                await this.fetchClientOrderNoDropDownItems(initialClientDropDownValue)

            const newSchema = [...this.state.schema]
            const [clientDropDown, clientOrderNoDropDown] = newSchema

            clientDropDown.items = [...clientDropDownItems]
            clientOrderNoDropDown.items = [...clientOrderNoDropDownItems]

            this.setState({
                schema: newSchema,
                clientOrderNoDropDownItemsIndex: {
                    [initialClientDropDownValue]: [...clientOrderNoDropDownItems]
                },
                selectedClient: initialClientDropDownValue,
            })
        }
        fetchClientDropDownItems() 
    }

    componentWillUnmount() {
        localStorage.getItem('clientCodeDropDownItems') ||
        localStorage.setItem(
            'clientCodeDropDownItems',
            JSON.stringify(this.state.schema[0].items)
        )

        localStorage.getItem('clientOrderNoDropDownItemsIndex') ||
        localStorage.setItem(
            'clientOrderNoDropDownItemsIndex',
            JSON.stringify(this.state.clientOrderNoDropDownItemsIndex)
        )

        localStorage.getItem('selectedClient')
        localStorage.setItem(
            'selectedClient',
            JSON.stringify(this.state.selectedClient)
        )
    }

    handleClientCodeDropDownChange(e) {
        const asyncWrapper = async () => {
            const selectedClient = e.target.value
            const orderNoItems = await this.fetchOrderNoDropDownItems(selectedClient)
            const newSchema = [...this.state.schema]
            const orderNoDropDown = newSchema[1]
            orderNoDropDown.items = [...orderNoItems]

            this.setState({
                schema: newSchema,
                selectedClient: selectedClient,
                clientOrderNoDropDownItemsIndex: {
                    ...this.state.clientOrderNoDropDownItemsIndex,
                    [selectedClient]: orderNoItems, 
                }
            })
        }

        asyncWrapper()
    }

    async fetchClientOrderNoDropDownItems(clientCode) {
        const res = await fetch(`/api/clientOrderNo?clientCode=${clientCode}`)
        const data = await res.json()
        const transformedData = transform(data, (res, item) => {
            const modified = {
                id   : item.id,
                name : `${item.client_order_no} (${item.part.part_no})`,
                value: `${item.client_order_no}`
            }

            res.push(modified)
        })

        return transformedData
    }



    render() {
        return (
            <Form
                name="오더생성 및 MAT-Label생성"
                schema={this.state.schema}
                onSubmit={this.props.onSubmit}
                submitName="생성하기"
            />
        )
    }
}
/** #####LAGACY CODE##### 
let schema_ = [{
    name: 'clientCode',
    prettyName: '고객사',
    type: 'dropDown',
    items: [],
}, {
    name: 'clientOrderNo',
    prettyName: '고객사 주문번호 (Order No.)',
    type: 'dropDown',
    itmes: [],
}, {
    name: 'shipNote',
    prettyName: '송장번호 (Shipping Note)',
    type: 'text',
    regex: /^[a-z0-9]*$/
}, {
    name: 'quantity',
    prettyName: '주문수량',
    type: 'number',
    regex: /^[0-9]*$/
}, {
    name: 'manDate',
    prettyName: '납품제품 생산일',
    type: 'date',
}]

const CreateOrderForm = ({onSubmit}) => {

    const [schema, setSchema] = useState(schema_ )

    useEffect(() => {
        if (schema[0].items.length === 0) {
            async function fetchDropDownItems() {
                const res = await fetch('/api/client')
                const data = await res.json()

                _.each(data, (item) => {
                    item.name = `${item.name} (${item.code})`
                    item.value = item.code
                    delete item.code
                })

                const newSchema = [...schema]
                newSchema[0].items = [...data]

                setSchema(newSchema)
            }
            fetchDropDownItems()
        
            return () => {
                schema_ = schema
            }
        }

        if (schema[1].items.length === 0) {
            async function fetchDropDownItems() {
                const res = await fetch('/api/part')
                const data = await res.json()

                _.each(data, (item) => {
                    item.name = `${item.name} (${item.part_no})`
                    item.value = item.part_no
                    delete item.part_no
                })

                const newSchema = [...schema]
                newSchema[1].items = [...newSchema[1].items, ...data]

                setSchema(newSchema)
            }
            fetchDropDownItems()
        
            return () => {
                schema_ = schema
            }
        }
    }, [schema])

    return (
        <Form name="오더등록 및 DMC생성" schema={schema} onSubmit={onSubmit} submitName="생성하기" />
    )
}
*/

export default CreateOrderForm

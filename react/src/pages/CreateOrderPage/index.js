import React from 'react'
import { connect } from 'react-redux'
import CreateOrderForm from './CreateOrderForm'

import { createDMCOrder } from '../../actions'

import { toISOStringWithTz } from '../../utils'


class CreateOrderPage extends React.PureComponent {
    render() {
        return (
            <CreateOrderForm onSubmit={this.props.onSubmit} />
        )
    }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    onSubmit: (e) => {
        const formData = new FormData(e.target)
        const dataObj = Object.fromEntries(formData)

        //: Convert %Y-%m-%d date format into ISO Datetime format 
        dataObj.manDate = toISOStringWithTz(new Date(dataObj.manDate))
        dataObj.quantity = parseInt(dataObj.quantity, 10)

        const jsonData = JSON.stringify(dataObj)

        dispatch(createDMCOrder.request({ body: jsonData, history: ownProps.history }))
    }
})

export default connect(
    null,
    mapDispatchToProps
)(CreateOrderPage)


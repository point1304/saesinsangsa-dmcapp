import React from 'react'
import { Route } from 'react-router-dom'

import DMCsByOrderId from './DMCsByOrderId'
import QueryDMCsByOrderIdForm from './QueryDMCsByOrderIdForm'

class DMCsPage extends React.PureComponent {
    render() {
        const { path } = this.props.match
        return (
            <>
                <Route exact path={`${path}`} component={QueryDMCsByOrderIdForm} />
                <Route path={`${path}/:orderId`} component={DMCsByOrderId} />
            </>
        )
    }
}

export default DMCsPage
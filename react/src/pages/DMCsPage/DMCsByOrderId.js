import React, { useEffect } from 'react'

import { connect } from 'react-redux'

import { Button }from '../../components/Buttons'
import DMCLabelListContainer from '../../containers/DMCLabelListContainer'

import { fetchDMCByOrderId } from '../../actions'

const GetDMCsByOrderId = ({
    match,

    selectedOrder,
    didFetch,
    dmcLabelData,
    getDMCsByOrderId,
}) => {    
    useEffect(() => {
        getDMCsByOrderId()
    }, [getDMCsByOrderId])

    //: Fallback to the loading page.
    //: Even for the faction of seconds of re-rendering labels
    //: upon change of :state:`selectedOrder`.
    //: So checks if `params.orderId === selectedOrder`.
    //: Previous labels will flicker without this condition.
    if (didFetch && match.params.orderId === selectedOrder) {
        return (
            <div>
                <Button onClick={window.print} value={'프린트'}/>
                <DMCLabelListContainer dmcLabelData={dmcLabelData} />
            </div>
        )
        
    } else {
        return <div>{'Loading...'}</div>
    }
}

const mapStateToProps = (state) => {
    const dmcs = state.selectedOrder && state.dmcsByOrderId[state.selectedOrder] ?
                    state.dmcsByOrderId[state.selectedOrder] : null

    return {
        selectedOrder: state.selectedOrder,
        didFetch: dmcs ? dmcs.didFetch : null,
        dmcLabelData: dmcs ? dmcs.dmcLabels : null,
    }
}
const mapDispatchToProps = (dispatch, ownProps) => ({
    getDMCsByOrderId: () => {
        const orderId = ownProps.match.params.orderId
        dispatch(fetchDMCByOrderId.request({orderId}))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GetDMCsByOrderId)

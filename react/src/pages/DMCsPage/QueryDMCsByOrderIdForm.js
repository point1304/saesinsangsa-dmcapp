import React from 'react'
import { connect } from 'react-redux'

import each from 'lodash/each'
import transform from 'lodash/transform'

import Form from '../../components/Form'

import { fetchDMCByClientCodeAndOrderNo } from '../../actions'


class QueryDMCsForm extends React.PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            schema: [
                {
                    name: 'clientCode',
                    prettyName: '고객사',
                    type: 'dropDown',
                    onChange: this.handleClientCodeDropDownChange.bind(this),
                    items: []
                }, {
                    name: 'clientOrderNo',
                    prettyName: '고객사 주문번호 (Order No.)',
                    type: 'dropDown',
                    items: []
                },
                {
                    name: 'shipNote',
                    prettyName: 'ASN번호 (Shipping Note)',
                    type: 'text',
                    regex: /^[a-zA-Z0-9]*$/,
                    size: 10,
                },   
            ],
            clientOrderNoDropDownItemsIndex: {},
            selectedClient: null,
        }
    }

    componentDidMount() {
        if ( localStorage.getItem('clientCodeDropDownItems') ) {
            const cachedClientCodeDropDownItems = 
                JSON.parse(localStorage.getItem('clientCodeDropDownItems'))
            const cachedClientOrderNoDropDownItemsIndex =
                JSON.parse(localStorage.getItem('clientOrderNoDropDownItemsIndex'))
            const cachedSelectedClient =
                JSON.parse(localStorage.getItem('selectedClient'))

            const newSchema = this.state.schema

            newSchema[0].items = cachedClientCodeDropDownItems || []
            newSchema[1].items = cachedClientOrderNoDropDownItemsIndex &&
                                    cachedSelectedClient ?
                                    cachedClientOrderNoDropDownItemsIndex[cachedSelectedClient] :
                                    []
            
            this.setState({
                schema                         : newSchema,
                selectedClient                 : cachedSelectedClient,
                clientOrderNoDropDownItemsIndex: cachedClientOrderNoDropDownItemsIndex || {}
            })

            return
        }

        const fetchClientDropDownItems = async() => {
            const res = await fetch('/api/client')
            const clientDropDownItems = await res.json()

            each(clientDropDownItems, (item) => {
                item.name = `${item.name} (${item.code})`
                item.value = item.code
                delete item.code
            })

            const initialClientDropDownValue = clientDropDownItems[0].value
            const clientOrderNoDropDownItems = 
                await this.fetchClientOrderNoDropDownItems(initialClientDropDownValue)

            const newSchema = [...this.state.schema]
            const [clientDropDown, clientOrderNoDropDown] = newSchema

            clientDropDown.items = [...clientDropDownItems]
            clientOrderNoDropDown.items = [...clientOrderNoDropDownItems]

            this.setState({
                schema: newSchema,
                clientOrderNoDropDownItemsIndex: {
                    [initialClientDropDownValue]: [...clientOrderNoDropDownItems]
                },
                selectedClient: initialClientDropDownValue,
            })
        }
        fetchClientDropDownItems() 
    }

    componentWillUnmount() {
        localStorage.getItem('clientCodeDropDownItems') ||
        localStorage.setItem(
            'clientCodeDropDownItems',
            JSON.stringify(this.state.schema[0].items)
        )

        localStorage.getItem('clientOrderNoDropDownItemsIndex') ||
        localStorage.setItem(
            'clientOrderNoDropDownItemsIndex',
            JSON.stringify(this.state.clientOrderNoDropDownItemsIndex)
        )

        localStorage.getItem('selectedClient')
        localStorage.setItem(
            'selectedClient',
            JSON.stringify(this.state.selectedClient)
        )
    }

    handleClientCodeDropDownChange(e) {
        const asyncWrapper = async () => {
            const selectedClient = e.target.value
            const orderNoItems = await this.fetchOrderNoDropDownItems(selectedClient)
            const newSchema = [...this.state.schema]
            const orderNoDropDown = newSchema[1]

            orderNoDropDown.items = [...orderNoItems]

            this.setState({
                schema: newSchema,
                selectedClient: selectedClient,
                clientOrderNoDropDownItemsIndex: {
                    ...this.state.clientOrderNoDropDownItemsIndex,
                    [selectedClient]: orderNoItems, 
                }
            })
        }

        asyncWrapper()
    }

    async fetchClientOrderNoDropDownItems(clientCode) {
        const res = await fetch(`/api/clientOrderNo?clientCode=${clientCode}`)
        const data = await res.json()
        const transformedData = transform(data, (res, item) => {
            const modified = {
                id   : item.id,
                name : `${item.client_order_no} (${item.part.part_no})`,
                value: `${item.client_order_no}`
            }

            res.push(modified)
        })

        return transformedData
    }

    render() {
        return (
            <Form
                name       = "DMC 조회"
                schema     = {this.state.schema}
                onSubmit   = {this.props.onSubmit}
                submitName = "조회하기"
            />
        )
    }
}
/** ####### LAGACY CODE ######
//: caching
let schema_ = [
    {
        name: 'clientCode',
        prettyName: '고객사',
        type: 'dropDown',
        items: []
    },
    {
        name: 'orderNo',
        prettyName: 'OrderNumber',
        type: 'dropDown',
        items: []
    },
    {
        name: 'shipNote',
        prettyName: 'Shipping Note',
        type: 'text',
        regex: /^[a-zA-Z0-9]*$/,
        size: 10,
    },
]
const QueryDMCsForm = ({onSubmit}) => {

    const [schema, setSchema] = useState(schema_ )

    useEffect(() => {
        if (schema[0].items.length === 0) {
            async function fetchDropDownItems() {
                const res = await fetch('/api/client')
                const data = await res.json()

                _.each(data, (item) => {
                    item.name = `${item.name} (${item.code})`
                    item.value = item.code
                    delete item.code
                })

                const newSchema = [...schema]
                newSchema[0].items = [...data]

                setSchema(newSchema)
            }
            fetchDropDownItems()
        
            return () => {
                schema_ = schema
            }
        }
    }, [schema])

    return <Form name="DMC 조회" schema={schema} onSubmit={onSubmit} submitName="조회하기" />
}
*/

const mapDispatchToProps = (dispatch, ownProps) => ({
    onSubmit: (e) => {
        const formData = new FormData(e.target)

        const clientCode = formData.get('clientCode')
        const clientOrderNo = formData.get('clientOrderNo')
        const shippingNote = formData.get('shipNote')

        dispatch(
            fetchDMCByClientCodeAndOrderNo.request({
                clientCode,
                clientOrderNo,
                shippingNote,
                history: ownProps.history
            })
        )
    }
})

export default connect(
    null,
    mapDispatchToProps
)(QueryDMCsForm)

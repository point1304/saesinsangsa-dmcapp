import { keyMirror } from './utils'

function makeAsyncActions(actionName) {
    const prefix = actionName
    return keyMirror(prefix, {
        INDEX: null,
        REQUEST: null,
        SUCCESS: null,
        FAIL: null,
    })
}

export const actionTypes = {
    FETCH_DMC_BY_ORDER_ID: makeAsyncActions('FETCH_DMC_BY_ORDER_ID'),
    CREATE_DMC_ORDER: makeAsyncActions('CREATE_DMC_ORDER'),

    FETCH_DMC_BY_CLIENT_CODE_AND_ORDER_NO: makeAsyncActions('FETCH_DMC_BY_CLIENT_CODE_AND_ORDER_NO'),

    LOGIN: makeAsyncActions('LOGIN'),
    LOGOUT: makeAsyncActions('LOGOUT'),
    LOGIN_PING: makeAsyncActions('LOGIN_PING')
}

function makeActionCreator(actionType) {
    return payload => ({ type: actionType, payload })
}

function makeAsyncActionCreator(actions) {
    const ret = makeActionCreator(actions.INDEX)
    ret.request = makeActionCreator(actions.REQUEST)
    ret.success = makeActionCreator(actions.SUCCESS)
    ret.fail = makeActionCreator(actions.FAIL)

    return ret
}

export const fetchDMCByOrderId =
    makeAsyncActionCreator(actionTypes.FETCH_DMC_BY_ORDER_ID)

export const createDMCOrder = 
    makeAsyncActionCreator(actionTypes.CREATE_DMC_ORDER)

export const fetchDMCByClientCodeAndOrderNo =
    makeAsyncActionCreator(actionTypes.FETCH_DMC_BY_CLIENT_CODE_AND_ORDER_NO)

export const login =
    makeAsyncActionCreator(actionTypes.LOGIN)

export const logout =
    makeAsyncActionCreator(actionTypes.LOGOUT)

export const loginPing =
    makeAsyncActionCreator(actionTypes.LOGIN_PING)
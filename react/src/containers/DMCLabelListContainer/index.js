import React from 'react'
import DMCLabelList from '../../components/DMCLabelList'

const DMCLabelContainer = ({dmcLabelData}) => {
    dmcLabelData = dmcLabelData.map((val, idx) => {
        const dmcData = {
            id: val.id,
            dmcSrc: val.dmc_src,
            dmcData: {
                'part No.': val.client_part_no,
                'Ord. Code': val.part_no,
                'Man. Date': val.man_date_short,
                'Exp. Date': val.exp_date_short,
                'Add. Info': val.add_info,
                'Supplier ID': val.given_supplier_code,
                'part Name': val.part_name,
                'Quantity': val.pretty_unit,
                'Index': val.index || '0',
                '1.Batch': val.batch_code,
                '2.Batch': val.batch_code_2,
                'MS-Level': val.ms_level,
                'Package ID': val.package_id,
                'Supplier': val.supplier || 'SAESINSANGSA',
                'Man. Loc': val.man_loc,
                'Man. Part No.': val.part_no,
                'Purchase': val.client_order_no,
                'Shipping Note': val.shipping_note,
                'Supplier Data': val.supplier_data,
            }
        }
        return dmcData
    })
    return (
        <DMCLabelList dmcLabelData={dmcLabelData} />
    )
}

export default DMCLabelContainer
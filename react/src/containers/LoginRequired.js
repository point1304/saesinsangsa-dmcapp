import React from 'react'
import { connect } from 'react-redux'

import AuthPage from '../pages/AuthPage'
import { loginPing } from '../actions'


const Spinner = () => (
    <div>{'Loading....'}</div>
)
const RouteDecorator = (Component) => {
    class Inner extends React.PureComponent {
        /*
        componentDidMount() {
            const { isLoggedIn, loginPing } = this.props
            console.log('didMount')
            console.log( isLoggedIn )
            if ( isLoggedIn ) return
            else {
                console.log('dispatch loginPing')
                loginPing()
            }
        }*/
        componentDidMount() {
            if ( ! this.props.isLoggedIn ) {
                this.props.checkLogin()
            }
        }

        render() {
            const { isLoggedIn, didCheckLogin, isCheckingLogin } = this.props
            return (
                <>
                    {
                        isCheckingLogin && ! didCheckLogin ?
                        <Spinner /> :
                        isLoggedIn ?
                        <Component {...this.props} /> :
                        <>
                            <div style={{color: 'red'}}>{'로그인이 필요한 페이지입니다.'}</div>
                            <AuthPage />
                        </>
                    }
                </>
            )
        }
    }
    const mapStateToProps = (state) => ({
        isLoggedIn: state.isLoggedIn,
        didCheckLogin: state.didCheckLogin,
        isCheckingLogin: state.isCheckingLogin
    })
    const mapDispatchToProps = (dispatch) => ({
        checkLogin: () => {
            dispatch(loginPing.request())
        }
    })
    
    const ret = connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Inner)

    return ret
}

export default RouteDecorator

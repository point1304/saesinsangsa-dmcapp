import React from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'

import { NavBar, Header } from '../../components/PageComponents'
import NavButton from '../../components/NavButton'

import { logout } from '../../actions'

import styles from './index.module.scss'

class NavBarContainer extends React.PureComponent {
    constructor(props) {
        super(props)

        this.universalNavLinks = [{
            to: '/createOrder',
            name: '오더생성'
        },
        {
            to: '/dmcs',
            name: 'DMC 조회'
        },
        {
            to: '/deleteOrder',
            name: '오더삭제'
        },
        {
            to: '/createClient',
            name: '고객사등록'
        },
        {
            to: '/createPart',
            name: '품번생성'
        },

        {
            to: '/createClientPart',
            name: '품번연결'
        }]

        this.forLoggedInUserNavLinks = []
    }
    
    render() {
        const { isLoggedIn, logout } = this.props

        return (
            <Header>
                <div className={styles.wrap} >
                    <Link to='/' className={styles.ci}>{'세신상사'}</Link>
                    <NavBar
                        links={
                            !isLoggedIn ? 
                            this.universalNavLinks :
                            this.universalNavLinks.concat(this.forLoggedInUserNavLinks) 
                        }
                    />
                    <div className={styles.userBtn} >
                    {
                        isLoggedIn ?
                        <NavButton name="로그아웃" onClick={logout} /> :
                        null
                    }
                    </div>
                </div>
            </Header>
        )
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: state.isLoggedIn
})
const mapDispatchToProps = (dispatch, ownProps) => ({
    logout: () => {
        dispatch(logout.request({ history: ownProps.history }))
    }
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(NavBarContainer))

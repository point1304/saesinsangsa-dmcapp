#!/bin/bash
docker build . --tag myapp-test
docker run -v /home/point1304/projects/test-sock:/tmp/uwsgi \
-v /home/point1304/projects/dmcapp/backend:/var/www/html \
-p 5001:5001 --cap-add=SYS_PTRACE \
-e RDS_HOSTNAME=172.17.0.1 \
-e RDS_DB_NAME=saeshin_dmc \
-e RDS_USERNAME=dev \
-e RDS_PASSWORD=dev \
-e SECRET_KEY=point \
--sysctl net.core.somaxconn=1024 \
--name myapp-test myapp-test
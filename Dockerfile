FROM point1304/dmcapp-base:0.1
LABEL version="1.0"
LABEL maintainer="point1304@gmail.com"

# uwsgi stat server
EXPOSE 5001

WORKDIR /var/www/html

ENTRYPOINT ["/bin/bash"]
CMD        ["setup.sh"]
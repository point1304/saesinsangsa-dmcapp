<Caveats>

1. Deployment
    1.1 ElasticBeanstalk(referred to "eb" hereinafter) doesn't set up the
        whole eb environment from scratch if it's not your first deployment
        to the environment.

        It overwrites files on to the existing file system of your ec2 instance
        and just run the startup script there, which makes lots of headaches
        specifically in those cases where you are trying to delete files from
        the current project structure.

        e.g. Let's say you made a config file to play with some nginx settings,
            like "/etc/nginx/conf.d/some.conf". And If you decided to delete 
            "some.conf" from your projects for some reason and hit "eb deploy"
            command after deleting it from your local machine, you would have
            no luck in seeing the file deleted on your eb ec2 instance.
            
        This may cause a serious deployment accident, especially in the case
        where you belive you have validated a new version of your deployment
        in an actual eb setup by making a fresh eb environment to test your
        deployment before you go for real.
        
        The possible solution, which this boilerplate app has adopted, is
        initialize any config-related directory with some code snippets in
        `/.ebextionsions/eb-bootstrap.config`, which enables users to run
        initialization scripts upon deployment.

2. Cronjob
    2.1 Running cronjobs is a big pain point in such distributed an environment
        like elasticbeanstalk, where you end up having several duplicated
        applications under the ELB layer. It means setting a cronjob in an
        application level may cause the job to run multiple times by multiple
        EC2 instances.

        ElasticBeanstalk, however, offers 2 neat approaches with different
        level of complexity which you can pick up one from depending on your
        needs.

        1) Worker Environment
            cron.yaml with SQS queue. Off-load jobs to daemon worker environment
            which secures cronjobs being executed only once powered by eb's own
            proprietary leader instance detection algorithm.

        2) Linux-level cron script
            Using leader-only option.
            :CAVEATS: Leader instance must be protected from being terminated
            by auto-scaling.
import os

DEBUG = False
SECRET_KEY = os.environ['SECRET_KEY']
DATABASE = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
    os.environ['RDS_USERNAME'],
    os.environ['RDS_PASSWORD'],
    os.environ['RDS_HOSTNAME'],
    os.environ['RDS_PORT'],
    os.environ['RDS_DB_NAME'],
)
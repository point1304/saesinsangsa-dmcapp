#!/bin/bash
addgroup --system --gid 497 nginx
adduser --system --no-create-home --uid 498 --gid 497 --shell /usr/sbin/nologin nginx

if [ ! -d "/var/log" ]; then
    mkdir /var/log
fi

if [ ! -d "/var/log/uwsgi" ]; then
    mkdir /var/log/uwsgi
fi

touch /var/log/uwsgi/access.log
touch /var/log/uwsgi/error.log
chown -R nginx:nginx /var/log/uwsgi
chmod -R 770 /var/log/uwsgi
chmod 660 /var/log/uwsgi/access.log
chmod 660 /var/log/uwsgi/error.log

if [ ! -d "/tmp/uwsgi" ]; then
    mkdir /tmp/uwsgi
fi

chown -R nginx:nginx /tmp/uwsgi
chmod -R 775 /tmp/uwsgi

uwsgi /var/www/html/uwsgi.ini

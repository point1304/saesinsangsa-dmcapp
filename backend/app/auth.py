from flask import (
    Blueprint, request, session, jsonify, g, Response 
)
from werkzeug.security import check_password_hash

from .models import *
from .util import login_required

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/login', methods=('POST',))
def login():
    form = request.get_json()

    username = form.get('username')
    password = form.get('password')

    user = User.query.filter_by(name=username).one_or_none()

    if user is None:
        return '', 404
    
    elif not check_password_hash(user.enc_password, password):
        return '', 401

    session.clear()
    session['username'] = user.name

    return '', 200

@bp.route('/logout')
def logout():
    session.clear()
    return '', 200

@bp.route('/ping')
@login_required
def ping():
    return '', 200

@bp.before_app_request
def load_logged_in_user():
    username = session.get('username')

    if username is None:
        g.user = None
    
    else:
        g.user = username
        #g.user = User.query.filter_by(name = username).one_or_none()

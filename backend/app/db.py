from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session


db_session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
    )
)

def get_engine(app):
    return create_engine(
        app.config['DATABASE'],
        convert_unicode = True,
        echo = app.config['DEBUG'],
        pool_size = 35,
        max_overflow = 5,
    )

def init_sqlalchemy(app):
    engine = get_engine(app)
    
    db_session.configure(bind=engine)

    from .models import Base
    Base.query = db_session.query_property()
    Base.metadata.create_all(engine)

    #: Commandline interface
    ### EMPTY ###

    app.teardown_appcontext(shutdown_session)


    return engine

def shutdown_session(e=None):
    if e is not None:
        db_session.rollback()
    db_session.remove()

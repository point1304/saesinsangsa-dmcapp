import time
from io import BytesIO

from secrets import choice
from base64 import b64encode
from functools import wraps
from collections import OrderedDict

from datetime import datetime
from pytz import timezone
from dateutil.relativedelta import relativedelta

from PIL import Image

from flask import request, abort, g, make_response
from pylibdmtx import pylibdmtx as dmtx

UTC = timezone('utc')
KST = timezone('Asia/Seoul')

def iso_quantity_to_normal(unit):
    if 'KGM' in unit:
        return str(float(unit.replace('KGM', '.'))) + 'KG'

def iso_datetime_format_to_datetime(v):
    #: 2019-05-25T:19:29:30.290119+09:00
    if len(v) == 32:
        ret = datetime.strptime(
                        v[:-3] + v[-2:],
                        '%Y-%m-%dT%H:%M:%S.%f%z'
                    ).astimezone(UTC)

    #: 2019-05-25T:19:29:30+09:00
    elif len(v) == 25:
        ret = datetime.strptime(
                        v[:-3] + v[-2:],
                        '%Y-%m-%dT%H:%M:%S%z'
                    ).astimezone(UTC)
        
    return ret

def str_to_relativedelta(string):
    size = int(string[:len(string) - 1])
    unit = string[-1]
    if unit == 'M':
        return relativedelta(months=size)
    elif unit == 'Y':
        return relativedelta(years=size)
    elif unit == 'D':
        return relativedelta(days=size)
    else:
        raise TypeError('Invalid string format')

def gen_rand_str(length, charset='ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'):
    return ''.join(choice(charset) for _ in range(length))

def get_dmtx_b64encoded(data):
    #: Convert raw image bytes into a `JPEG` formatted bytes by using
    #: `PIL.Image.save()`.
    #: This function fakes a file system by making a `file-like object`
    #: using `BytesIO` as a stream buffer.
    dmtx_enc = dmtx.encode(data.encode('utf8'))
    buffered = BytesIO()

    img = Image.frombytes('RGB', (dmtx_enc.width, dmtx_enc.height), dmtx_enc.pixels)
    img.save(buffered, format='JPEG')

    b64_enc = b64encode(buffered.getvalue())

    return (b'data:image/jpg;base64,' + b64_enc).decode()

def get_bosch_dmtx_string(
    bosch_part_no,
    supplier_part_no,
    supplier_duns_no,
    man_loc,
    man_date,
    exp_date,
    rohs,
    ms_level,
    purchase_no,
    shipping_note,
    supplier_code,
    package_id,
    unit,
    ord_code=None,
    index='0',
    add_info='',
    batch_counter='1',
    batch_code_1='TESTVALUE',
    batch_code_2='',
    supplier_data='',
    label_version='0002',
):
    data = {
        "prefix": "[)>@06",
        "label_version": {
            "id": "@12S",
            "val": label_version,
        },
        "part_no": {
            "id": "@P",
            "val": bosch_part_no,
        },
        "man_part_no": {
            "id": "@1P",
            "val": supplier_part_no,
        },
        "ord_code": {
            "id": "@31P",
            "val": ord_code if ord_code is not None else supplier_part_no,
        },
        "man_no": {
            "id": "@12V",
            "val": supplier_duns_no,
        },
        "man_loc": {
            "id": "@10V",
            "val": man_loc,
        },
        "index": {
            "id": "@2P",
            "val": index,
        },
        "add_info": {
            "id": "@20P",
            "val": add_info
        },
        "man_date": {
            "id": "@6D",
            "val": man_date,
        },
        "exp_date": {
            "id": "@14D",
            "val": exp_date,
        },
        "rohs": {
            "id": "@30P",
            "val": rohs,
        },
        "ms_level": {
            "id": "@Z",
            "val": ms_level,
        },
        "purchase": {
            "id": "@K",
            "val": purchase_no,
        },
        "shipping_note": {
            "id": "@16K",
            "val": shipping_note,
        },
        "supplier_id": {
            "id": "@V",
            "val": supplier_code,
        },
        "package_id": {
            "id": "@3S",
            "val": package_id,
        },
        "quantity": {
            "id": "@Q",
            "val": unit,
        },
        "batch_counter": {
            "id": "@20T",
            "val": batch_counter,
        },
        "batch_no_1": {
            "id": "@1T",
            "val": batch_code_1,
        },
        "batch_no_2": {
            "id": "@2T",
            "val": batch_code_2,
        },
        "supplier_data": {
            "id": "@1Z",
            "val": supplier_data,
        },
        "suffix": "@@"
    }

    return _extract_data(data)

def _extract_data(obj):
    data_string = ''
    if isinstance(obj, dict):
        for val in obj.values():
            data_string += _extract_data(val)
    elif isinstance(obj, (list, tuple)):
        for val in obj:
            data_string += _extract_data(val)
    elif isinstance(obj, str):
        data_string += obj
    elif isinstance(obj, float):
        data_string += format(obj, '.2f')
    elif isinstance(obj, int):
        data_string += str(obj)
    elif isinstance(obj, datetime):
        data_string += obj.strftime('%Y%m%d')
    elif obj is None:
        pass
    else:
        raise TypeError('Not supported datatype %s' % type(obj))
    
    return data_string

def login_required(view):
    @wraps(view)
    def inner(**kwargs):
        if g.user is None:
            return '', 401
        
        return view(**kwargs)
    
    return inner

def json_required(args=[]):
    def outer(view):
        @wraps(view)
        def inner(**kwargs):
            if request.method == 'POST':
                POST = request.get_json()

                if POST is None or not has_required_keys(POST.keys(), args):
                    abort(400)
            
            return view(**kwargs)
        return inner
    return outer

def q_string_required(args=[]):
    def outer(view):
        @wraps(view)
        def inner(**kwargs):
            if request.method == 'GET':
                if request.url == request.base_url:
                    abort(404)

                else:
                    if all([isinstance(el, str) for el in args]):
                        if not has_required_keys(request.args.keys(), args):
                            abort(404)
                    
                    elif all([isinstance(el, (tuple, list)) for el in args]):
                        if not any([has_required_keys(request.args.keys(), args_opt) for args_opt in args]):
                            abort(404)

                    else:
                        raise TypeError('invalid input for `args=[]` in :func:`q_string_required`')
            
            return view(**kwargs)
        return inner
    return outer

def has_required_keys(keys, required):
    return all([el in keys for el in required])

def parse_float_or_return_self(s):
    try:
        return float(s)
    except ValueError:
        return s

class cached_property(object):
    def __init__(self, ttl=300):
        self.ttl = ttl
    
    def __call__(self, fget, doc=None):
        self.fget = fget
        self.__doc__ = doc or fget.__doc__
        self.__name__ = fget.__name__
        self.__module__ = fget.__module__
        return self
    
    def __get__(self, inst, owner):
        now = time.time()
        try:
            value, last_update = inst._cache[self.__name__]
            if self.ttl > 0 and now - last_update > self.ttl:
                raise AttributeError
        
        except (KeyError, AttributeError):
            value = self.fget(inst)
            try:
                cache = inst._cache
            except AttributeError:
                cache = inst._cache = {}

            cache[self.__name__] = (value, now)

        return value

def timeout_cache(ttl=300, maxsize=8):
    #: Algorithmically, OrderedDict can handle frequent 
    #: reordering operations better than dict. This makes
    #: it suitable for tracking recent accesses (e.g. LRU cache).
    _cache = OrderedDict()
    def outer(f):
        @wraps(f)
        def inner(*args):
            now = time.time()
            try:
                res, last_updated = _cache[(f, *args)]
                if ttl > 0 and now - last_updated > ttl:
                    raise AttributeError
            except (KeyError, AttributeError):
                res = f(*args)
                #: TODO: this is not thread safe..
                if len(_cache) >= maxsize - 1:
                    _cache.popitem()

                _cache[(f, *args)] = (res, now)

            return res

        return inner
    return outer

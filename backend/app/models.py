import re
import json

from datetime import datetime, timedelta

from sqlalchemy import (
    func,
    Column,
    String,
    Integer,
    DECIMAL,
    Boolean,
    Interval,
    ForeignKey,
    UniqueConstraint,
    ForeignKeyConstraint,
)
from sqlalchemy_utc import UtcDateTime, utcnow
from sqlalchemy.orm import relationship, joinedload

from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method

from sqlalchemy.orm.attributes import QueryableAttribute
from sqlalchemy.exc import IntegrityError, OperationalError

from werkzeug.security import check_password_hash, generate_password_hash

from .util import (
    gen_rand_str,
    get_bosch_dmtx_string,
    get_dmtx_b64encoded,
    timeout_cache,
    UTC,
    KST,
    iso_quantity_to_normal,
    iso_datetime_format_to_datetime,
    parse_float_or_return_self,
)
from .db import db_session

__all__ = [
    'Base',
    'CompanyInfo',
    'User',
    'Client',
    'Part',
    'PartClientMap',
    'OrderUnit',
    'Order',
    'Package',
]
#: Helper functions for :Models: should come first before they are declared.
#: Will definetely throw exceptions if you put helpers on bottom.
def _gen_client_code():
    """Helper function for setting defualt client code.
    This func can't be encapsulated into `:Model:Client` as
    column default function will be evaluated in the
    initialization phase of `:Model:` and an encapsulated function
    will never have been declared by the time it's being evaluated.
    """
    def query_code(code):
        return Client.query.filter_by(code=code).one_or_none()

    code = gen_rand_str(5)
    isUniq = query_code(code)
    while isUniq is not None:
        code = gen_rand_str(5)
        isUniq = query_code(code)
    
    return code

class BaseDeclarativeMeta(DeclarativeMeta):

    def __new__(cls, clsname, bases, dict_):
        __datetime_cols__ = dict_.setdefault('__datetime_cols__', [])
        __interval_cols__ = dict_.setdefault('__interval_cols__', [])

        for k, v in dict_.items():
            if not k.startswith('__') and isinstance(v, Column):
                if isinstance(v.type, UtcDateTime):
                    __datetime_cols__.append(k)
                
                elif isinstance(v.type, Interval):
                    __interval_cols__.append(k)
        
        return super().__new__(cls, clsname, bases, dict_)

class BaseModelMixIn(object):
    """:MixIn: which grants a `:Model:` capability to convert its query result
    into a `:dict` object. The name of the function(`_asdict`) is after 
    the same function of `:sqlalchemy.util._collections.result:` which is
    a `row-equivalent-object` of a query result with its columns to present
    being specified at the time of query like
    `session.query(Model.id, Model.name).filter_by ...` 
    """
    def __init__(self, **kwargs):
        """An __init__ implementation which extends sqlalchemy's default
        :param:`constructor` for :func:`declarative_base`. 
        :func:`declarative_base falls back to this implemetation by setting
        :param:`constructor to `None`. It's important to note that this 
        implementation only works with :metaclass:`BaseDeclarativeMeta`
        being fed as :param:`metaclass` of :func:`declarative_base`.

        example usage>

        Base = declarative_base(
            cls         = BaseModelMixIn,
            metaclass   = BaseDeclarativeMeta,
            constructor = None
        )

        Extended Features>
        This __init__ implementation grants an sqlalchemy model capability to
        accept `str` inputs in a relavant format for `UtcDateTime` and `Interval`
        type columns. A `str` input is then be parsed and converted either into
        python `datetime` or `timedelta` objects depending on which datatype the
        columns declaraed in the model.

        The following formats are supported for `str` inputs.
        >> UtcDateTime : `%Y-%m-%dT%H:%M:%S.%f%z` 
                         isoformat time expression with timezone info.
                         (e.g. ``2019-05-16T12:32:10.21099+09:00``)
        >> Interval    : `%d days, %H:%M:%S.%f`
                         (e.g. ``10 days, 12:36:10.14200``)
        """
        #: Can it be decoupled with :class:`BaseDeclarativeMeta`?
        #: by changing its method to access column type into 
        #: `self.__table__.columns`.

        #: kwargs adapter which convert non-compatible kwargs into
        #: proper ones for the sake of convenience of API.
        kwargs = self.kwargs_adapter(**kwargs)

        cls_ = type(self)
        for k, v in kwargs.items():
            if not hasattr(cls_, k):
                raise TypeError(
                    '%r is an invalid keward argument for %s' % (k, cls_.__name__)
                )
            
            if k in self.__datetime_cols__ and isinstance(v, str):
                
                #TODO: Use util/:func:iso_datetime_format_to_datetime
                #: 2019-05-25T:19:29:30.290119+09:00
                if len(v) == 32:
                    kwargs[k] = datetime.strptime(
                                    v[:-3] + v[-2:],
                                    '%Y-%m-%dT%H:%M:%S.%f%z'
                                ).astimezone(UTC)

                #: 2019-05-25T:19:29:30+09:00
                elif len(v) == 25:
                    kwargs[k] = datetime.strptime(
                                    v[:-3] + v[-2:],
                                    '%Y-%m-%dT%H:%M:%S%z'
                                ).astimezone(UTC)

            if k in self.__interval_cols__ and isinstance(v, str):
                parsed = re.split('\s|:|,', v)
                parsed = [parse_float_or_return_self(el) for el in parsed]

                days, hours, minutes, seconds, microseconds = [0] * 5
                if len(parsed) == 2:
                    days, _ = parsed

                elif len(parsed) == 6:
                    days, _, hours, minutes, seconds = parsed

                elif len(parsed) == 3:
                    hours, minutes, seconds = parsed

                elif len(parsed) == 6:
                    days, _, hours, minutes, seconds, microseconds = parsed

                kwargs[k] = timedelta(
                    days=days,
                    hours=hours,
                    minutes=minutes,
                    seconds=seconds,
                    microseconds=microseconds
                )

            setattr(self, k, kwargs[k])

    def _asdict(self):
        return {key: getattr(self, key) for key in self.__mapper__.c.keys()}
    
    def to_dict(self, show=[], hide=[], path=None):
        """Return a dictionary representation of this model."""

        hidden = getattr(self, '__hidden_cols__', [])
        default = getattr(self, '__default_cols__', [])
        default.extend(['id',])

        if not path:
            path = self.__tablename__.lower()

            def attach_path_prefix(item):
                item = item.lower()
                if item.split('.', 1)[0] == path:
                    return item
                if len(item) == 0:
                    return item
                if not item.startswith('.'):
                #TODO : following 2 lines can be merged into ``item = '%s.%s' % (path, item)``.
                    item = '.%s' % item
                item = '%s%s' % (path, item)
                return item
            
            hide = [attach_path_prefix(el) for el in hide]
            show = [attach_path_prefix(el) for el in show]

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)

        ret_data = {}

        for key in columns:
            if key.startswith('_'):
                continue

            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue

            if check in show or key in default:
                ret_data[key] = getattr(self, key)
        
        for key in relationships:
            if key.startswith('_'):
                continue
            
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            
            if check in show or key in default:
                hide.append(check)

                is_list = self.__mapper__.relationships[key].uselist
                if is_list:
                    items = getattr(self, key)
                    if self.__mapper__.relationships[key].query_class is not None:
                        if hasattr(items, 'all'):
                            items = items.all()

                    for item in items:
                        ret_data.setdefault(key, []).append(
                            item.to_dict(
                                show=show[:],
                                hide=hide[:],
                                path=('%s.%s' % (path, key.lower())),
                            )
                        )
                else:
                    if (
                        self.__mapper__.relationships[key].query_class is not None or
                        self.__mapper__.relationships[key].instrument_class is not None
                    ):
                        item = getattr(self, key)
                        if item is not None:
                            ret_data[key] = item.to_dict(
                                show=show[:],
                                hide=hide[:],
                                path=('%s.%s' % (path, key.lower())),
                            )

                        else:
                            ret_data[key] = None

                    else:
                        ret_data[key] = getattr(self, key)
        
        for key in list(set(properties) - set(columns) - set(relationships)):
            if key.startswith('_'):
                continue

            if not hasattr(self.__class__, key):
                continue
            
            attr = getattr(self.__class__, key)
            if not isinstance(attr, (property, QueryableAttribute)):
                continue

            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            
            if check in show or key in default:
                val = getattr(self, key)
                if hasattr(val, 'to_dict'):
                    ret_data[key] = val.to_dict(
                        show=show[:],
                        hide=hide[:],
                        path=('%s.%s' % (path, key.lower())),
                    )
                
                else:
                    try:
                        # ret_data[key] = json.loads(json.dumps(val))
                        ret_data[key] = str(val)
                    except:
                        pass
        
        return ret_data

    @classmethod
    def _get_or_create(
        cls,
        _session=None,
        _filters=None,
        _defaults={},
        _retry_count=0,
        _max_retries=3,
        **kwargs
    ):
        if _session is None:
            _session = db_session
        
        query = _session.query(cls)
        if _filters is not None:
            query = query.filter(*_filters)
        if len(kwargs) > 0:
            query = query.filter_by(**kwargs)
        
        instance = query.first()
        
        if instance is not None:
            return instance, False
        
        _session.begin_nested()

        try:
            kwargs.update(_defaults)
            instance = cls(**kwargs)
            _session.add(instance)
            _session.commit()
            return instance, True
        
        except IntegrityError:
            _session.rollback()
            instance = query.first()
            if instance is None:
                raise
            return instance, False
        
        except OperationalError:
            _session.rollback()
            instance = query.first()
            if instance is None:
                if _retry_count < _max_retries:
                    return cls._get_or_create(
                        _filters=_filters,
                        _defaults=_defaults,
                        _retry_count=_retry_count + 1,
                        _max_retries=_max_retries,
                        **kwargs
                    )
                raise
            return instance, False
    
    @classmethod
    def get_or_create(cls, **kwargs):
        kwargs = cls.kwargs_adapter(**kwargs)
        return cls._get_or_create(**kwargs)[0]

    @classmethod
    def kwargs_adapter(cls, **kwargs):
        return kwargs

    def __repr__(self):
        return repr(self._asdict())


Base = declarative_base(cls=BaseModelMixIn, metaclass=BaseDeclarativeMeta, constructor=None)

class CompanyInfo(Base):
    __tablename__ = 'company_info'

    id = Column(Integer, primary_key=True)

    option_name = Column(String(20))
    option_value = Column(String(50))

    __table_args__ = (
        UniqueConstraint('option_name', name='uk_option_name'),
    )

class User(Base):
    __tablename__ = 'user'
    
    id = Column(Integer, primary_key=True)
    created_at = Column(UtcDateTime, default=utcnow())
    
    name = Column(String(45), nullable=False)
    enc_password = Column(String(100), nullable=False)
    email = Column(String(45), nullable=False)

    __table_args__ = (
        UniqueConstraint('name', name='uk_user_name'),
    )

    @hybrid_property
    def password(self):
        return self.enc_password

    @password.setter
    def password(self, password):
        self.enc_password = generate_password_hash(password)

    @hybrid_method
    def is_matched_password(self, password):
        return check_password_hash(self.enc_password, password)

class Client(Base):
    __tablename__ = 'client'

    id = Column(Integer, primary_key=True)
    created_at = Column(UtcDateTime, default=utcnow())

    name = Column(String(50))
    code = Column(String(5), nullable=False, default=_gen_client_code)
    given_code = Column(String(13))
    
    part_client_map = relationship(
        'PartClientMap',
        order_by='PartClientMap.id',
        back_populates='client',
        innerjoin=True
    )

    __default_cols__ = ['name', 'code']

    __table_args__ = (
        UniqueConstraint('code', name='uk_code'),
        UniqueConstraint('name', name='uk_client_name'),
    )

class Part(Base):
    __tablename__ = 'part'
    
    id = Column(Integer, primary_key=True)
    created_at = Column(UtcDateTime, default=utcnow())
    updated_at = Column(UtcDateTime, default=utcnow(), onupdate=utcnow())

    part_no = Column(String(13), nullable=False)
    name = Column(String(30), nullable=False)
    man_loc = Column(String(20))

    unit = Column(String(10))
    expire_in = Column(Interval, default=timedelta(days=3650))
    index = Column(String(2))
    rohs = Column(Boolean)
    ms_level = Column(String(1))

    part_client_map = relationship(
        'PartClientMap',
        order_by='PartClientMap.id',
        back_populates='part',
        innerjoin=True
    )

    __default_cols__ = ['name', 'part_no']

    __table_args__ = (
        UniqueConstraint('part_no', name='uk_part_no'),
    )

class PartClientMap(Base):
    __tablename__ = 'part_client_map'

    id = Column(Integer, primary_key=True)
    created_at = Column(UtcDateTime, default=utcnow())
    updated_at = Column(UtcDateTime, default=utcnow(), onupdate=utcnow())

    fk_part_id = Column(Integer, ForeignKey('part.id'), nullable=False)
    fk_client_id = Column(Integer, ForeignKey('client.id'), nullable=False)

    client_part_no = Column(String(13))
    
    part = relationship(
        'Part',
        back_populates='part_client_map',
        innerjoin=True
    )
    client = relationship(
        'Client',
        back_populates='part_client_map',
        innerjoin=True
    )
    order_unit = relationship(
        'OrderUnit',
        back_populates='part_client_map',
        order_by='OrderUnit.id',
        innerjoin=True
    )
    
    __table_args__ = (
        UniqueConstraint('fk_client_id', 'fk_part_id',
                        name='uk_fk_client_fk_part_id'),
        UniqueConstraint('fk_client_id', 'client_part_no',
                        name='uk_fk_client_id_client_part_no'),
    )

class OrderUnit(Base):
    __tablename__ = 'order_unit'

    id = Column(Integer, primary_key=True)
    created_at = Column(UtcDateTime, default=utcnow())
    updated_at = Column(UtcDateTime, default=utcnow(), onupdate=utcnow())

    fk_part_client_map_id = Column(Integer,
                                ForeignKey('part_client_map.id'),
                                nullable=False)

    client_order_no = Column(String(18), nullable=False, default='')
    packing_type = Column(String(2), default='00')
    shipping_type = Column(String(2), default='00')
    price = Column(DECIMAL(15, 2))

    part_client_map = relationship(
        'PartClientMap',
        back_populates='order_unit',
        innerjoin=True
    )
    order = relationship(
        'Order',
        back_populates='order_unit',
        order_by='Order.id',
        innerjoin=True
    )

    __default_cols__ = [
        'client_order_no',
        'part',
        'shipping_type',
        'packing_type',
    ]

    __table_args__ = (
        UniqueConstraint(
            'fk_part_client_map_id',
            'packing_type',
            'shipping_type',
            name='uk_fk_part_client_map_id_packing_type_shipping_type'
        ),
    )

    @classmethod
    def kwargs_adapter(cls, **kwargs):
        client_code = kwargs.pop('client_code', None)
        part_no     = kwargs.pop('part_no', None)

        if client_code is not None and part_no is not None and 'fk_part_client_map_id' not in kwargs:
            kwargs['fk_part_client_map_id'] = PartClientMap.query \
                .filter(
                    PartClientMap.part.has(Part.part_no == part_no),
                    PartClientMap.client.has(Client.code == client_code)
                ) \
                .with_entities(PartClientMap.id) \
                .as_scalar()

        return kwargs

    @property
    def part(self):
        return self.part_client_map.part

    @property
    def client(self):
        return self.part_client_map.client

    @hybrid_property
    def order_no(self):
        return self.part.part_no + '/' + \
                self.client.code + self.packing_type + self.shipping_type
    
    @order_no.expression
    def order_no(cls):
        return cls.part_client_map.part.part_no + '/' + \
                cls.part_client_map.client.code + cls.packing_type + cls.shipping_type

class Order(Base):
    __tablename__ = 'order'

    id = Column(Integer, primary_key=True)
    created_at = Column(UtcDateTime, default=utcnow())
    updated_at = Column(UtcDateTime, default=utcnow(), onupdate=utcnow())

    #fk_part_client_map_id = Column(Integer, ForeignKey('part_client_map.id'), nullable=False)
    fk_order_unit_id = Column(
                        Integer, ForeignKey('order_unit.id'), nullable=False)

    released_at = Column(UtcDateTime)
    delivered_at = Column(UtcDateTime)

    quantity = Column(Integer, nullable=False)
    shipping_note = Column(String(12), nullable=False)
    #client_order_no = Column(String(18), nullable=False)

    batch_code = Column(
        String(17),
        nullable=False,
        default=datetime.now(tz=UTC).strftime('%Y%m%d') + gen_rand_str(7)
    )

    order_unit = relationship(
        'OrderUnit',
        back_populates='order',
        innerjoin=True
    )
    package = relationship(
        'Package',
        back_populates='order',
        order_by='Package.id',
        innerjoin=True,
    )

    __default_cols__ = ['released_at', 'delivered_at', 'quantity', 'shipping_note']

    __table_args__ = (
        #UniqueConstraint('fk_part_client_map_id########', 'client_order_no',
        #                name='uk_fk_part_client_map_id_client_order_no'),
        UniqueConstraint('fk_order_unit_id', 'shipping_note',
                        name='uk_fk_order_unit_id_shipping_note'),
    )

    @classmethod
    def kwargs_adapter(cls, **kwargs):
        client_code = kwargs.pop('client_code', None)
        client_order_no = kwargs.pop('client_order_no', None)

        if client_code is not None and \
            client_order_no is not None and \
            'fk_order_unit_id' not in kwargs:
                
            kwargs['fk_order_unit_id'] = OrderUnit.query \
                .join('part_client_map', 'client') \
                .filter(
                    OrderUnit.client_order_no == client_order_no,
                    Client.code               == client_code,
                ) \
                .with_entities(OrderUnit.id) \
                .as_scalar()

        return kwargs

    @property
    def client(self):
        return self.order_unit.client
    
    @property
    def part(self):
        return self.order_unit.part

    @property
    def part_client_map(self):
        return self.order_unit.part_client_map

    @hybrid_property
    def order_no(self):
        return self.order_unit.order_no

    @order_no.expression
    def order_no(cls):
        return OrderUnit.order_no

    @order_no.setter
    def order_no(self, value):
        self.fk_order_unit_id = \
            OrderUnit.query.with_entities(OrderUnit.id).filter_by(
                order_no = value
            ).as_scalar()

    @hybrid_property
    def client_order_no(self):
        return self.order_unit.client_order_no

    @client_order_no.expression
    def client_order_no(cls):
        return OrderUnit.client_order_no

def _gen_rand_package_id():
    return 'S' + gen_rand_str(12)

class Package(Base):
    __tablename__ = 'package'

    id = Column(Integer, primary_key=True)
    fk_order_id = Column(Integer, ForeignKey('order.id'), nullable=False)
    # [WARN] default function of :column:`package_id` doesn't gurantee universal
    # uniqueness of generated strings. This will rarely cause an :exc:`IntegrityError`
    # which should appropriately handled at the time of insertion queries.
    package_id = Column(String(13), nullable=False, default=_gen_rand_package_id)

    man_date = Column(UtcDateTime, default=utcnow())

    order = relationship('Order', back_populates='package', innerjoin=True)

    __default_cols__ = [
        'part_no',
        'client_part_no',
        'man_date_short',
        'exp_date_short',
        'given_supplier_code',
        'part_name',
        'pretty_unit',
        'ms_level',
        'package_id',
        'man_loc',
        'client_order_no',
        'shipping_note',
        'dmc_src',
        'batch_code',
    ]

    __table_args__ = (
        UniqueConstraint('package_id', name='uq_package_id'),
    )

    @property
    def dmc_src(self):
        data = get_bosch_dmtx_string(
            supplier_duns_no = get_duns_no(),
            bosch_part_no = self.client_part_no,
            supplier_part_no = self.part_no,
            man_loc = self.man_loc,
            man_date = self.man_date_short,
            exp_date = self.exp_date_short,
            rohs = self.rohs,
            ms_level = self.ms_level,
            purchase_no = self.client_order_no,
            shipping_note = self.shipping_note,
            supplier_code = self.given_supplier_code,
            package_id = self.package_id,
            unit = self.unit,
            batch_code_1 = self.batch_code,
        )

        return get_dmtx_b64encoded(data)

    @property
    def man_date_short(self):
        return self.man_date.astimezone(KST).strftime('%Y%m%d')
    
    @property
    def exp_date_short(self):
        return self.exp_date.astimezone(KST).strftime('%Y%m%d')

    @property
    def pretty_unit(self):
        return iso_quantity_to_normal(self.unit)

    @hybrid_property
    def batch_code(self):
        return self.order.batch_code
    
    @batch_code.expression
    def batch_code(cls):
        return Order.batch_code

    @hybrid_property
    def exp_date(self):
        return self.man_date + self.order.part.expire_in 

    @exp_date.expression
    def exp_date(cls):
        return cls.man_date + Part.expire_in

    @hybrid_property
    def client_order_no(self):
        return self.order.client_order_no
    
    @client_order_no.expression
    def client_order_no(cls):
        return Order.client_order_no
    
    @hybrid_property
    def client_part_no(self):
        return self.order.part_client_map.client_part_no
    
    @client_part_no.expression
    def client_part_no(cls):
        return PartClientMap.client_part_no

    @hybrid_property
    def shipping_note(self):
        return self.order.shipping_note
    
    @shipping_note.expression
    def shipping_note(cls):
        return Order.shipping_note
    
    @hybrid_property
    def part_no(self):
        return self.order.part.part_no
    
    @part_no.expression
    def part_no(cls):
        return Part.part_no
    
    @hybrid_property
    def part_name(self):
        return self.order.part.name
    
    @part_name.expression
    def part_name(cls):
        return Part.name
    
    @hybrid_property
    def unit(self):
        return self.order.part.unit
    
    @unit.expression
    def unit(cls):
        return Part.unit
    
    @hybrid_property
    def man_loc(self):
        return self.order.part.man_loc
    
    @man_loc.expression
    def man_loc(cls):
        return Part.man_loc
    
    @hybrid_property
    def rohs(self):
        if self.order.part.rohs:
            return 'Y'
        elif self.order.part.rohs == False:
            return 'N'
        elif self.order.part.rohs is None:
            return '0'
    
    @rohs.expression
    def rohs(cls):
        return Part.rohs

    @hybrid_property
    def ms_level(self):
        return self.order.part.ms_level
    
    @ms_level.expression
    def ms_level(cls):
        return Part.ms_level

    @hybrid_property
    def client_name(self):
        return self.order.client.name
    
    @client_name.expression
    def client_name(cls):
        return Client.name

    @hybrid_property
    def given_supplier_code(self):
        return self.order.client.given_code

    @given_supplier_code.expression
    def given_supplier_code(cls):
        return Client.given_code

@timeout_cache(ttl=1)
def get_duns_no():
    """Query :str:`duns_no` from :model:`ComapnyInfo`.
    resultant string will be cached for 10 min to keep
    this function from touching db multiple times only
    to get the same result.
    """
    return db_session \
            .query(CompanyInfo.option_value) \
            .filter_by(option_name='duns_no') \
            .scalar()

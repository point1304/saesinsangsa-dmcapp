from datetime import timedelta
from flask.json import JSONEncoder

class JSONEncoderExtended(JSONEncoder):

    def default(self, o):
        if isinstance(o, timedelta):
            return str(o)

        return super().default(o)
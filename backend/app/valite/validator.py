import re

def String(length=None, strict=False, longer_than=False, f=None):

    def inner(val):
        if f and not f(val):
            return False

        if length is None and strict:
            raise TypeError(
                '`strict` = True option is not valid '
                'when `length` is None'
            )
        if not isinstance(val, str):
            return False

        if length is not None:
            return len(val) == length if \
                    strict else \
                    len(val) <= length if \
                    not longer_than else \
                    len(val) >= length

    return inner

def EqualTo(value):
    def validator(val):
        return value == val

    return validator

def Except(value):
    def validator(val):
        return value != val

    return validator

def Regex(regex):
    def inner(val):
        pattern = re.compile(regex)
        match = pattern.fullmatch(val)
        return True if match else False

    return inner

Email = Regex('^\S+@\S+\.\S+$')

def or_(*validators):
    def inner(val):
        return any([validator(val) for validator in validators])

    return inner
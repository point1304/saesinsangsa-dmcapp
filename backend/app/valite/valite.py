import weakref
from functools import partial

class ValidationError(Exception):
    pass

class MissingAttributeError(Exception):
    pass

class MiddlewareManager(object):
    __slots__ = ['_middleware_manager']
    def __init__(self):
        self._middleware_manager = _MiddlewareManager()

    def __get__(self, obj, owner):
        self._middleware_manager._caller_weakref = weakref.ref(obj)
        return self._middleware_manager

    def use(self, fn):
        return self._middleware_manager.use(fn)

class _MiddlewareManager(object):
    def __init__(self):
        self._middlewares = []
        self._form = None
    
    def use(self, fn):
        self._middlewares.append(fn)
    
    def _run(self, idx, err=None):
        if idx < 0 or idx >= len(self._middlewares):
            return
            
        next_mw = self._middlewares[idx]
        ####
        next_mw._caller_weakref = self._caller_weakref
        ####
        next_tic = partial(self._run, idx + 1)

        if err is not None:
            if issubclass(next_mw.__class__, BaseErrorMiddleware):
                return next_mw(self._form, next_tic, err)
            else:
                return self._run(idx + 1, err)
        
        next_mw(self._form, next_tic)
    
    def run(self, form):
        if not isinstance(form, dict):
            raise TypeError(
                ":args:`form` should be a dict obj")

        print(form)
        self._form = form
        self._run(0)

    @property
    def __caller__(self):
        return self._caller_weakref()

class BaseErrorMiddleware(object):
    def __call__(self, form, next_tic, err=None):
        if err is not None:
            raise err

class Validate(object):
    __slots__ = ['_attr', 'validators', 'required', '_caller_weakref']

    def __init__(self, *validators, required=True):
        self.validators = validators
        self.required = required

    def __call__(self, form, next_tic, err=None):
        val = form.pop(self._attr, None)

        if val is None:
            if not self.required:
                return next_tic()

            else:
                err = MissingAttributeError(
                    'Required attribute is missing '
                    '(attr: %s, input: %s)' % \
                    (self._attr, val)
                )
                return next_tic(err=err)

        if (
            val is not None and
            all([validator(val) for validator in self.validators])
        ):
            ###
            self.__caller__._data[self._attr] = val
            ###
            next_tic()
        
        else:
            err = ValidationError(
                'validation failed for (attr: %s, input: %s)' % \
                (self._attr, val)
            )
            next_tic(err=err)

    @property
    def __caller__(self):
        return self._caller_weakref()

class ValiteMeta(type):
    def __new__(cls, clsname, bases, dict_):
        if not getattr(dict_, '__abstract__', False):
            middlewares = MiddlewareManager()
            
            for attr in dict_:
                if isinstance(dict_[attr], Validate):
                    dict_[attr]._attr = attr
                    middlewares.use(dict_[attr])

            dict_.update({
                '_middlewares': middlewares,
            })

        return type.__new__(cls, clsname, bases, dict_)

class Valite(metaclass=ValiteMeta):
    __abstract__ = True
    
    ErrorMiddleware = BaseErrorMiddleware()

    def __init__(self):
        self._form = {}
        self._data = {}
        self._middlewares.use(self.ErrorMiddleware)

    def validate(self):
        self._middlewares.run(self._form)
        #: Method Chaining
        return self

    def from_dict(self, dict_):
        self._form.update(**dict_)
        #: Method Chaining
        return self

    @property
    def data(self):
        return self._data
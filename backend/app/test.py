from datetime import datetime, timezone

from sqlalchemy import create_engine, Column, String, Integer, DateTime, ForeignKey, UniqueConstraint, select, case, func
from sqlalchemy.orm import sessionmaker, relationship, joinedload, scoped_session
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy_utc import UtcDateTime
from sqlalchemy.ext.hybrid import hybrid_property

engine = create_engine('sqlite:///:memory:')

s = scoped_session(sessionmaker(bind=engine))
print(s.query_property)


class BaseDeclarativeMeta(DeclarativeMeta):

    def __new__(cls, clsname, bases, dict_):
        __datetime_cols__ = dict_.setdefault('__datetime_cols__', [])

        for key, val in dict_.items():
            if not key.startswith('__') and isinstance(val, Column):
                if isinstance(val.type, UtcDateTime):
                    __datetime_cols__.append(key)

        return super().__new__(cls, clsname, bases, dict_)

class BaseModelMixin(object):
    def __init__(self, **kwargs):
        cls_ = type(self)
        for k, v in kwargs.items():
            if not hasattr(cls_, k):
                raise TypeError(
                    '%r is an invalid keward argument for %s' % (k, cls_.__name__)
                )
            
            if k in self.__datetime_cols__ and isinstance(v, str):
                kwargs[k] = datetime.strptime(
                                v[:-3] + v[-2:],
                                '%Y-%m-%dT%H:%M:%S.%f%z'
                            )
            setattr(self, k, kwargs[k])


Base = declarative_base(cls=BaseModelMixin, metaclass=BaseDeclarativeMeta, constructor=None)
print(Base)

class User(Base):
    __tablename__ = 'user'

    id      = Column(Integer, primary_key=True)
    name    = Column(String(10), nullable=False)
    created = Column(UtcDateTime, nullable=False)

    test    = relationship(
        'Test',
        back_populates='user',
        uselist=False,
        innerjoin=True
    )

class Test(Base):
    __tablename__ = 'test'

    id         = Column(Integer, primary_key=True)
    fk_user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    test_val   = Column(String(11))

    user       = relationship(
        'User',
        back_populates='test',
        innerjoin=True
    )

    __table_args__ = (
        UniqueConstraint('test_val', name='uk_test_val'),
    )

    @hybrid_property
    def test_value(self):
        if self.test_val:
            return self.test_val
        else:
            return self.user.name + 'HELLO'

    @test_value.expression
    def test_value(cls):
        return case(
            [
                (cls.test_val != None, cls.test_val),
            ],
            else_ = User.name + 'HELLO'
        )

    @hybrid_property
    def username(self):
        return self.user.name
    
    @username.expression
    def username(cls):
        return User.name
    
    @username.setter
    def username(self, value):
        self.fk_user_id = User.query.with_entities(User.id).filter_by(name=value).as_scalar()

    @hybrid_property
    def hello(self):
        return self.test_val + self.world

    @hello.expression
    def hello(cls):
        return cls.test_val + cls.world

    @hybrid_property
    def world(self):
        return self.test_val

    @world.expression
    def world(cls):
        return cls.test_val

Base.query = s.query_property()
Base.metadata.create_all(engine)

user = User(created=datetime.now(tz=timezone.utc).isoformat(), name='KS')
user.test = Test(test_val=None)
s.add(user)
s.commit()
print(user.created)
print(user.test.test_value)

user2 = User(created=datetime.now(tz=timezone.utc).isoformat(), name='HELLO')
user2.test = Test(test_val='KKK')
s.add(user2)
s.commit()


test = Test(test_val=None, username='KS')
s.add(test)
s.commit()

for el in s.query(Test).options(joinedload('user')).filter(Test.user.has(User.name == 'KS')).all():
    print(el.fk_user_id)

for el in s.query(Test).filter(Test.user.has(Test.test_value== 'KSHELLO')).all():
    print(el.fk_user_id)

for el in s.query(Test).filter_by(hello='KKKKKK').all():
    print(el.hello)
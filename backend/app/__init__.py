import os

from flask import Flask, request
from .db import init_sqlalchemy

# development only...
_allowed_origins = ['http://localhost:5000', 'http://127.0.0.1:5000']
def _allow_cors(res):
    res.headers['Access-Control-Allow-Origin'] = request.environ['HTTP_ORIGIN'] if request.environ['HTTP_ORIGIN'] in _allowed_origins else '*'
    res.headers['Access-Control-Allow-Credentials'] = 'true'
    if request.method == 'OPTIONS':
        res.headers['Access-Control-Allow-Methods'] = 'DELETE, GET, POST, PUT, OPTIONS'
        headers = request.headers.get('Access-Control-Request-Headers')
        if headers:
            res.headers['Access-Control-Allow-Headers'] = headers

    return res

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY = 'dev',
        DATABASE   = os.path.join(app.instance_path, 'dev.sqlite')
    )

    try:
        os.makedirs(app.instance_path)
    #: silence the error in case `app.instance_path` already exists.
    except OSError:
        pass

    if test_config is not None:
        app.config.from_mapping(test_config)

    else:
        try:
            flask_env = os.environ['FLASK_ENV']
        except KeyError:
            app.config.from_pyfile('.'.join(['development', 'config', 'py']))
    
        else:
            app.config.from_pyfile('.'.join([flask_env, 'config', 'py']))
            if flask_env != 'production':
                pass
                # app.after_request(_allow_cors)

    engine = init_sqlalchemy(app)

    from .ext import JSONEncoderExtended
    app.json_encoder = JSONEncoderExtended

    from . import main
    app.register_blueprint(main.bp)
    app.add_url_rule('/', endpoint='index')

    from . import auth
    app.register_blueprint(auth.bp)

    #: Add cli commands
    from .command import init_db_command, drop_db_command
    app.cli.add_command(init_db_command)
    app.cli.add_command(drop_db_command)

    # hack for uwsgi - db connection issue with multiple workers
    # copy-on-write friendly gc hack
    try:
        from uwsgidecorators import postfork
        import gc
        postfork(engine.dispose)
        postfork(gc.enable)
    except ImportError:
        pass

    return app

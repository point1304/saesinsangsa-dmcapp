from datetime import datetime, timedelta
from pytz import timezone
from dateutil.relativedelta import relativedelta

from flask import Blueprint, url_for, jsonify, request, abort

from sqlalchemy import and_
from sqlalchemy.orm import joinedload, subqueryload, contains_eager
from sqlalchemy.exc import IntegrityError

from .models import * # pylint: disable=unused-wildcard-import
from .db import db_session as db

from .util import (
    get_bosch_dmtx_string, get_dmtx_b64encoded, UTC,
    json_required, q_string_required, login_required, gen_rand_str,
    iso_datetime_format_to_datetime
)

bp = Blueprint('main', __name__)

@bp.route('/')
def index():
    return '', 200

@bp.route('/slowApi')
def slow_api():
    res = db.execute('SELECT pg_sleep(1.0)')
    return jsonify('hello world!')

@bp.route('/dmcs', methods=('GET',))
def get_dmcs():
    order_id = request.args.get('orderId')

    res = Package.query \
            .options(joinedload('order').joinedload('order_unit').joinedload('part_client_map').joinedload('part', 'client')) \
            .filter(Package.order.has(Order.id == order_id)).order_by(Package.id).all()
        
    res = [el.to_dict() for el in res]

    if len(res) == 0:
        abort(404)

    return jsonify(res)

@bp.route('/order', methods=('GET', 'POST'))
@login_required
def get_or_post_order():
    if request.method == 'POST':
        form = request.get_json()

        order = Order(
            quantity        = int(form.get('quantity')),
            shipping_note   = form.get('shipNote'),
            released_at     = UTC.localize(datetime.strptime(form.get('orderDate'), '%Y%m%d')) if \
                                form.get('orderDate') is not None else \
                                datetime.now(tz=UTC),
            client_code     = form.get('clientCode'),
            client_order_no = form.get('clientOrderNo'),
            batch_code      = (
                                iso_datetime_format_to_datetime(form.get('manDate')) if \
                                form.get('manDate') is not None else \
                                datetime.now(tz=UTC)
                            ).strftime('%Y%m%d') + gen_rand_str(7)
        )

        order.package = [
            Package(
                man_date = form.get('manDate') if form.get('manDate') is not None else datetime.now(tz=UTC),
            ) for _ in range(form.get('quantity'))
        ]
        db.add(order) # pylint: disable=no-member; a scoped_session obj is dynamically converted into a session obj.
        db.commit()   # pylint: disable=no-member

        return jsonify(order.id), 201

@bp.route('/client', methods=('GET',))
@login_required
def get_client():
    res = Client.query.all()
    
    res = [el.to_dict() for el in res]

    return jsonify(res)

@bp.route('/part', methods=('GET',))
@login_required
def get_part():
    res = Part.query.all()

    res = [el.to_dict() for el in res]

    return jsonify(res)

@bp.route('/orderId', methods=('GET',))
@login_required
def get_order_id():
    client_order_no = request.args.get('orderNo')
    client_code = request.args.get('clientCode')
    shipping_note = request.args.get('shippingNote')

    #order = Order.query \
    #    .options(joinedload('part_client_map').joinedload('client')) \
    #    .filter(Client.code == client_code, Order.client_order_no == client_order_no) \
    #    .scalar()

    order_id = Order.query \
        .options(
            joinedload('order_unit') \
            .joinedload('part_client_map') \
            .joinedload('client')
        ) \
        .filter(
            Client.code == client_code,
            OrderUnit.client_order_no == client_order_no,
            Order.shipping_note == shipping_note
        ) \
        .with_entities(
            Order.id
        ) \
        .scalar()
            

    res = order_id
    
    return jsonify(res)

@bp.route('/clientOrderNo', methods=('GET',))
@login_required
def get_client_order_no():
    client_code = request.args.get('clientCode')

    # Why not using joinedload?
    res = OrderUnit.query \
            .join('part_client_map', 'client') \
            .filter(
                Client.code == client_code
            ) \
            .all()

    if len(res) is None:
        abort(404) 

    res = [el.to_dict() for el in res]

    return jsonify(res)

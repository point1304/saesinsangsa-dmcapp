import click
from flask import current_app
from flask.cli import with_appcontext

from sqlalchemy.exc import IntegrityError

from .db import get_engine, db_session as db
from .models import * # pylint: disable=unused-wildcard-import


@click.command('init-db')
@with_appcontext
def init_db_command():
    CompanyInfo.get_or_create(
        option_name='duns_no',
        option_value='687935891'
    )
    User.get_or_create(
        name='guest',
        email='guest@example.com',
        password='guest',
    )
    clients = ['Bosch Korea']
    for el in clients:
        if el == 'Bosch Korea':
            Client.get_or_create(
                name=el,
                given_code='97946912'
            )

    Part.get_or_create(
        part_no='D27110',
        name='SOLDER BAR HSE-02-B20',
        unit='20KGM000',
        expire_in='730 days',
        rohs=True,
        ms_level='N',
        man_loc ='KOR-YONGIN1'
    )
    db.commit()
    PartClientMap.get_or_create(
        fk_client_id=db.query(Client.id).filter_by(name='Bosch Korea').as_scalar(),
        fk_part_id=db.query(Part.id).filter_by(part_no='D27110').as_scalar(),
        client_part_no='7182000096',
    )
    db.commit()
    try:
        client_code = db.query(Client.code).filter_by(name='Bosch Korea').scalar()
        order_unit = OrderUnit.get_or_create(client_order_no='55390588/001', part_no='D27110', client_code=client_code)
        order_unit.order = [Order(shipping_note='TEST1', quantity=3), Order(shipping_note='TEST2', quantity=4)]
        for el in order_unit.order:
            el.package = [Package() for _ in range(el.quantity)]

        db.add(order_unit)
        db.commit()
    except IntegrityError:
        db.rollback()


    click.echo('db has been initialized')

@click.command('drop-db')
@with_appcontext
def drop_db_command():
    if current_app.config['ENV'] == 'development':
        Base.metadata.drop_all(get_engine(current_app))

        click.echo(
            'All app-related tables in db has been successfully dropped'
        )

    else:
        click.echo(
            'This app may be in production mode. `drop-db` can not be executed.'
        )

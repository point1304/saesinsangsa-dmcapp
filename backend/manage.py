import gc; gc.disable()

from psycogreen.gevent import patch_psycopg; patch_psycopg()
from gevent.monkey import patch_all; patch_all()
from app import create_app

app = create_app()

gc.freeze()

if __name__ == '__main__':
    app.run()